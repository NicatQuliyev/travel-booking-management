package com.nicat.travelbookingsystem;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
@RequiredArgsConstructor
@Slf4j
public class TravelBookingSystemApplication implements CommandLineRunner {


    public static void main(String[] args) {
        SpringApplication.run(TravelBookingSystemApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
//        PrimeNumber primeNumber = new PrimeNumber();
//        primeNumber.generatePrimeNumber();
//
//        Palindrome palindrome = new Palindrome();
//        palindrome.palindromeWord();
    }
}

