package com.nicat.travelbookingsystem.repository;

import com.nicat.travelbookingsystem.entity.Booking;
import com.nicat.travelbookingsystem.entity.User;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {
//    @Override
//    @EntityGraph(type = EntityGraph.EntityGraphType.FETCH,attributePaths = {"booking"})
//    Optional<User> findById(Long id);
}
