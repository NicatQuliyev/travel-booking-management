package com.nicat.travelbookingsystem.repository;

import com.nicat.travelbookingsystem.entity.Payment;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PaymentRepository extends JpaRepository<Payment, Long> {
}
