package com.nicat.travelbookingsystem.repository;

import com.nicat.travelbookingsystem.entity.Booking;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface BookingRepository extends JpaRepository<Booking, Long> {

//    @EntityGraph(type = EntityGraph.EntityGraphType.FETCH,attributePaths = {"students"})


//    @Override
//    @EntityGraph(type = EntityGraph.EntityGraphType.FETCH,attributePaths = {"booking"})
//    Optional<Booking> findById(Long id);
}
