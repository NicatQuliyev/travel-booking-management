package com.nicat.travelbookingsystem.repository;

import com.nicat.travelbookingsystem.entity.RentalCar;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RentalCarRepository extends JpaRepository<RentalCar, Long> {
}
