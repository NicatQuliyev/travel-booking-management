package com.nicat.travelbookingsystem.repository;

import com.nicat.travelbookingsystem.entity.Flight;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FlightRepository extends JpaRepository<Flight, Long> {
}
