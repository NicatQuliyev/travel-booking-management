package com.nicat.travelbookingsystem.service;

import com.nicat.travelbookingsystem.dto.request.HotelRequest;
import com.nicat.travelbookingsystem.dto.response.HotelResponse;

import java.util.List;

public interface HotelService {

    List<HotelResponse> findAll();

    HotelResponse findById(Long hotelId);

    HotelResponse save(Long bookingId, HotelRequest hotelRequest);

    HotelResponse update(Long hotelId, HotelRequest hotelRequest);

    void delete(Long hotelId);
}
