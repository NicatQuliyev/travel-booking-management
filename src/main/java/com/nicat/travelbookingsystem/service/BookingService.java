package com.nicat.travelbookingsystem.service;

import com.nicat.travelbookingsystem.dto.request.BookingRequest;
import com.nicat.travelbookingsystem.dto.response.BookingResponse;

import java.util.List;

public interface BookingService {

    List<BookingResponse> findAll();

    BookingResponse findById(Long bookingId);

    BookingResponse save(BookingRequest bookingRequest, Long paymentId, Long userId);

    BookingResponse update(BookingRequest bookingRequest, Long bookingId);

    void delete(Long bookingId);
}
