package com.nicat.travelbookingsystem.service;

import com.nicat.travelbookingsystem.dto.request.RentalCarRequest;
import com.nicat.travelbookingsystem.dto.response.RentalCarResponse;

import java.util.List;

public interface RentalCarService {

    List<RentalCarResponse> findAll();

    RentalCarResponse findById(Long rentalCarId);

    RentalCarResponse save(Long bookingId, RentalCarRequest rentalCarRequest);

    RentalCarResponse update(Long rentalCarId, RentalCarRequest rentalCarRequest);

    void delete(Long rentalCarId);
}
