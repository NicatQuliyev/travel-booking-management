package com.nicat.travelbookingsystem.service;

import com.nicat.travelbookingsystem.dto.request.UserRequest;
import com.nicat.travelbookingsystem.dto.response.UserResponse;

import java.util.List;

public interface UserService {

    List<UserResponse> findAll();

    UserResponse findById(Long userId);

    UserResponse save(UserRequest userRequest);

    UserResponse update(UserRequest userRequest, Long userId);

    void delete(Long userId);

}
