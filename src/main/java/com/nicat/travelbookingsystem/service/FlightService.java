package com.nicat.travelbookingsystem.service;

import com.nicat.travelbookingsystem.dto.request.FlightRequest;
import com.nicat.travelbookingsystem.dto.response.FlightResponse;

import java.util.List;

public interface FlightService {

    List<FlightResponse> findAll();

    FlightResponse findById(Long flightId);

    FlightResponse save(Long bookingId, FlightRequest flightRequest);

    FlightResponse update(Long flightId, FlightRequest flightRequest);

    void delete(Long flightId);
}
