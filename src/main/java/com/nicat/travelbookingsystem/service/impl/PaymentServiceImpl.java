package com.nicat.travelbookingsystem.service.impl;

import com.nicat.travelbookingsystem.dto.request.PaymentRequest;
import com.nicat.travelbookingsystem.dto.response.PaymentResponse;
import com.nicat.travelbookingsystem.entity.Payment;
import com.nicat.travelbookingsystem.repository.PaymentRepository;
import com.nicat.travelbookingsystem.service.PaymentService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class PaymentServiceImpl implements PaymentService {

    private final ModelMapper modelMapper;

    private final PaymentRepository paymentRepository;

    @Override
    public List<PaymentResponse> findAll() {
        return paymentRepository
                .findAll()
                .stream()
                .map(payment -> modelMapper.map(payment, PaymentResponse.class))
                .collect(Collectors.toList());
    }

    @Override
    @Cacheable(key = "#paymentId", cacheNames = "payment")
    public PaymentResponse findById(Long paymentId) {
        Payment payment = paymentRepository.findById(paymentId).orElseThrow(() -> new RuntimeException(
                String.format("Payment not found by id -%s", paymentId)
        ));

        return modelMapper.map(payment, PaymentResponse.class);
    }

    @Override
    public PaymentResponse save(PaymentRequest paymentRequest) {
        Payment payment = modelMapper.map(paymentRequest, Payment.class);

        return modelMapper.map(paymentRepository.save(payment), PaymentResponse.class);
    }

    @Override
    public PaymentResponse update(PaymentRequest paymentRequest, Long paymentId) {
        Payment payment = paymentRepository.findById(paymentId).orElseThrow(() -> new RuntimeException(
                String.format("Payment not found by id -%s", paymentId)
        ));

        modelMapper.map(paymentRequest, payment, "map");

        return modelMapper.map(paymentRepository.save(payment), PaymentResponse.class);
    }

    @Override
    public void delete(Long paymentId) {
        Payment payment = paymentRepository.findById(paymentId).orElseThrow(() -> new RuntimeException(
                String.format("Payment not found by id -%s", paymentId)
        ));

        paymentRepository.delete(payment);
    }
}
