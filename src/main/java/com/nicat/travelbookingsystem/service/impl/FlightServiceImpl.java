package com.nicat.travelbookingsystem.service.impl;

import com.nicat.travelbookingsystem.dto.request.FlightRequest;
import com.nicat.travelbookingsystem.dto.response.FlightResponse;
import com.nicat.travelbookingsystem.entity.Booking;
import com.nicat.travelbookingsystem.entity.Flight;
import com.nicat.travelbookingsystem.repository.BookingRepository;
import com.nicat.travelbookingsystem.repository.FlightRepository;
import com.nicat.travelbookingsystem.service.FlightService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class FlightServiceImpl implements FlightService {

    private final FlightRepository flightRepository;

    private final BookingRepository bookingRepository;

    private final ModelMapper modelMapper;

    private final CacheManager cacheManager;

    @Override
    public List<FlightResponse> findAll() {
        Cache cache = cacheManager.getCache("flightCache");
        Cache.ValueWrapper valueWrapper = cache.get("findAll");

        if (valueWrapper != null) {
            List<FlightResponse> cachedResult = (List<FlightResponse>) valueWrapper.get();
            if (cachedResult != null) {
                return cachedResult;
            }
        }
        List<FlightResponse> result = flightRepository
                .findAll()
                .stream()
                .map(flight -> modelMapper.map(flight, FlightResponse.class))
                .collect(Collectors.toList());

        cache.put("findAll", result);

        return result;
    }

    @Override
    public FlightResponse findById(Long flightId) {
        Cache cache = cacheManager.getCache("flight");
        Flight flightFromCache = cache.get(flightId, Flight.class);


        if (flightFromCache == null) {
            Flight flight = flightRepository.findById(flightId).orElseThrow(() -> new RuntimeException(
                    String.format("Flight not found by id -%s", flightId)
            ));

            cache.put(flightId, flight);

            return modelMapper.map(flight, FlightResponse.class);
        }

        return modelMapper.map(flightFromCache, FlightResponse.class);
    }

    @Override
    public FlightResponse save(Long bookingId, FlightRequest flightRequest) {
        Booking booking = bookingRepository.findById(bookingId).orElseThrow(() -> new RuntimeException(
                String.format("Booking not found by id -%s", bookingId)
        ));

        Flight flight = modelMapper.map(flightRequest, Flight.class);
        flight.setBooking(booking);

        return modelMapper.map(flightRepository.save(flight), FlightResponse.class);
    }

    @Override
    public FlightResponse update(Long flightId, FlightRequest flightRequest) {
        Cache cacheById = cacheManager.getCache("flight");
        Cache cacheAll = cacheManager.getCache("flightCache");

        cacheById.evict(flightId);

        cacheAll.evict("findAll");

        Flight flight = flightRepository.findById(flightId).orElseThrow(() -> new RuntimeException(
                String.format("Flight not found by id -%s", flightId)
        ));

        modelMapper.map(flightRequest, flight, "map");
        flight.setId(flightId);

        Flight updatedFlight = flightRepository.save(flight);

        cacheById.put(flightId, updatedFlight);

        List<Flight> updatedFlightList = flightRepository.findAll();
        cacheAll.put("findAll", updatedFlightList);

        return modelMapper.map(updatedFlight, FlightResponse.class);
    }
    /// Id cache update olur amma findAll olmur niye??

    @Override
    public void delete(Long flightId) {
        Cache cache = cacheManager.getCache("flight");
        Flight flightFromCache = cache.get(flightId, Flight.class);
//
//        Flight flight = flightRepository.findById(flightId).orElseThrow(() -> new RuntimeException(
//                String.format("Flight not found by id -%s", flightId)
//        ));
        if (flightFromCache == null) {
            flightFromCache = flightRepository.findById(flightId).get();
        }

        flightRepository.delete(flightFromCache);
        cache.evict(flightFromCache.getId());
    }
}
