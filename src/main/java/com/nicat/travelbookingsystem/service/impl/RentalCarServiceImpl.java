package com.nicat.travelbookingsystem.service.impl;

import com.nicat.travelbookingsystem.dto.request.RentalCarRequest;
import com.nicat.travelbookingsystem.dto.response.RentalCarResponse;
import com.nicat.travelbookingsystem.entity.Booking;
import com.nicat.travelbookingsystem.entity.RentalCar;
import com.nicat.travelbookingsystem.repository.BookingRepository;
import com.nicat.travelbookingsystem.repository.RentalCarRepository;
import com.nicat.travelbookingsystem.service.RentalCarService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class RentalCarServiceImpl implements RentalCarService {

    private final RentalCarRepository rentalCarRepository;

    private final BookingRepository bookingRepository;

    private final ModelMapper modelMapper;


    @Override
    public List<RentalCarResponse> findAll() {
        return rentalCarRepository
                .findAll()
                .stream()
                .map(rentalCar -> modelMapper.map(rentalCar, RentalCarResponse.class))
                .collect(Collectors.toList());
    }

    @Override
    @Cacheable(key = "#rentalCarId", cacheNames = "rental-car")
    public RentalCarResponse findById(Long rentalCarId) {
        RentalCar rentalCar = rentalCarRepository.findById(rentalCarId).orElseThrow(() -> new RuntimeException(
                String.format("Rental Car not found by id -%s", rentalCarId)
        ));

        return modelMapper.map(rentalCar, RentalCarResponse.class);

    }

    @Override
    public RentalCarResponse save(Long bookingId, RentalCarRequest rentalCarRequest) {
        Booking booking = bookingRepository.findById(bookingId).orElseThrow(() -> new RuntimeException(
                String.format("Booking not found by id -%s", bookingId)
        ));

        RentalCar rentalCar = modelMapper.map(rentalCarRequest, RentalCar.class);
        rentalCar.setBooking(booking);

        return modelMapper.map(rentalCarRepository.save(rentalCar), RentalCarResponse.class);
    }

    @Override
    public RentalCarResponse update(Long rentalCarId, RentalCarRequest rentalCarRequest) {
        RentalCar rentalCar = rentalCarRepository.findById(rentalCarId).orElseThrow(() -> new RuntimeException(
                String.format("Rental Car not found by id -%s", rentalCarId)
        ));

        modelMapper.map(rentalCarRequest, rentalCar, "map");
        rentalCar.setId(rentalCarId);

        return modelMapper.map(rentalCarRepository.save(rentalCar), RentalCarResponse.class);
    }

    @Override
    public void delete(Long rentalCarId) {
        RentalCar rentalCar = rentalCarRepository.findById(rentalCarId).orElseThrow(() -> new RuntimeException(
                String.format("Rental Car not found by id -%s", rentalCarId)
        ));

        rentalCarRepository.delete(rentalCar);
    }
}
