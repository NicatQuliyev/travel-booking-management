package com.nicat.travelbookingsystem.service.impl;

import com.nicat.travelbookingsystem.dto.request.BookingRequest;
import com.nicat.travelbookingsystem.dto.response.BookingResponse;
import com.nicat.travelbookingsystem.entity.Booking;
import com.nicat.travelbookingsystem.entity.Payment;
import com.nicat.travelbookingsystem.entity.User;
import com.nicat.travelbookingsystem.repository.BookingRepository;
import com.nicat.travelbookingsystem.repository.PaymentRepository;
import com.nicat.travelbookingsystem.repository.UserRepository;
import com.nicat.travelbookingsystem.service.BookingService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class BookingServiceImpl implements BookingService {

    private final BookingRepository bookingRepository;

    private final UserRepository userRepository;

    private final PaymentRepository paymentRepository;

    private final ModelMapper modelMapper;

    private final RedisTemplate<String, Object> redisTemplate;

    @Override
    public List<BookingResponse> findAll() {

        var bookingResponseFromCache = (List<BookingResponse>) redisTemplate.opsForValue().get("findAll");

        if (bookingResponseFromCache != null) {
            return bookingResponseFromCache;
        }

        List<BookingResponse> bookingResponses = bookingRepository
                .findAll()
                .stream()
                .map(booking -> modelMapper.map(booking, BookingResponse.class))
                .collect(Collectors.toList());

        redisTemplate.opsForValue().set("findAll", bookingResponses, Duration.ofSeconds(60000));

        return bookingResponses;
    }

    @Override
    public BookingResponse findById(Long bookingId) {
        var bookingResponseFromCache = (BookingResponse) redisTemplate.opsForValue().get(String.valueOf(bookingId));

        if (bookingResponseFromCache != null) {
            return bookingResponseFromCache;
        }

        Booking booking = bookingRepository.findById(bookingId).orElseThrow(() -> new RuntimeException(
                String.format("Booking not found by id -%s", bookingId)
        ));

        BookingResponse bookingResponse = modelMapper.map(booking, BookingResponse.class);

        redisTemplate.opsForValue().set(String.valueOf(bookingId), bookingResponse, Duration.ofSeconds(60000));

        return bookingResponse;
    }

    @Override
    public BookingResponse save(BookingRequest bookingRequest, Long paymentId, Long userId) {
        Payment payment = paymentRepository.findById(paymentId).orElseThrow(() -> new RuntimeException(
                String.format("Payment not found by id -%s", paymentId)
        ));

        User user = userRepository.findById(userId).orElseThrow(() -> new RuntimeException(
                String.format("User not found by id -%s", userId)
        ));

        Booking booking = modelMapper.map(bookingRequest, Booking.class);
        booking.setPayment(payment);
        booking.setUser(user);

        return modelMapper.map(bookingRepository.save(booking), BookingResponse.class);
    }

    @Override
    public BookingResponse update(BookingRequest bookingRequest, Long bookingId) {
        Booking booking = bookingRepository.findById(bookingId).orElseThrow(() -> new RuntimeException(
                String.format("Booking not found by id -%s", bookingId)
        ));

        modelMapper.map(bookingRequest, booking, "map");
        booking.setId(bookingId);

        BookingResponse bookingResponse = modelMapper.map(bookingRepository.save(booking), BookingResponse.class);

        redisTemplate.opsForValue().set(String.valueOf(bookingId), bookingResponse, Duration.ofSeconds(60000));

        return bookingResponse;
    }

    /// Id cache update olur amma findAll olmur muellimden sorusmaq

    @Override
    public void delete(Long bookingId) {
        Booking booking = bookingRepository.findById(bookingId).orElseThrow(() -> new RuntimeException(
                String.format("Booking not found by id -%s", bookingId)
        ));

        bookingRepository.delete(booking);

        redisTemplate.opsForValue().getAndDelete(String.valueOf(bookingId));
    }
}
