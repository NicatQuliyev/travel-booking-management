package com.nicat.travelbookingsystem.service.impl;

import com.nicat.travelbookingsystem.dto.request.UserRequest;
import com.nicat.travelbookingsystem.dto.response.UserResponse;
import com.nicat.travelbookingsystem.entity.User;
import com.nicat.travelbookingsystem.repository.UserRepository;
import com.nicat.travelbookingsystem.service.UserService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    private final ModelMapper modelMapper;

    @Override
    @Cacheable(cacheNames = "users", key = "#root.methodName")
    public List<UserResponse> findAll() {
        return userRepository
                .findAll()
                .stream()
                .map(user -> modelMapper.map(user, UserResponse.class))
                .collect(Collectors.toList());
    }

    @Override
    @Cacheable(key = "#userId", cacheNames = "user")
    public UserResponse findById(Long userId) {
        User user = userRepository.findById(userId).orElseThrow(() -> new RuntimeException(
                String.format("User not found by id -%s", userId)
        ));

        return modelMapper.map(user, UserResponse.class);
    }

    @Override
    public UserResponse save(UserRequest userRequest) {
        User user = modelMapper.map(userRequest, User.class);

        return modelMapper.map(userRepository.save(user), UserResponse.class);
    }

    @Override
    @Cacheable(cacheNames = "users", key = "#userId")
    public UserResponse update(UserRequest userRequest, Long userId) {
        userRepository.findById(userId).orElseThrow(() -> new RuntimeException(
                String.format("User not found by id -%s", userId)
        ));
        User responseUser = modelMapper.map(userRequest, User.class);
        responseUser.setId(userId);

        return modelMapper.map(userRepository.save(responseUser), UserResponse.class);

    }

    @Override
    @CacheEvict(cacheNames = "users", key = "#userId")
    public void delete(Long userId) {
        User user = userRepository.findById(userId).orElseThrow(() -> new RuntimeException(
                String.format("User not found by id -%s", userId)
        ));

        userRepository.delete(user);
    }
}
