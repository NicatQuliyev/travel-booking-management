package com.nicat.travelbookingsystem.service.impl;

import com.nicat.travelbookingsystem.dto.request.HotelRequest;
import com.nicat.travelbookingsystem.dto.response.HotelResponse;
import com.nicat.travelbookingsystem.entity.Booking;
import com.nicat.travelbookingsystem.entity.Hotel;
import com.nicat.travelbookingsystem.repository.BookingRepository;
import com.nicat.travelbookingsystem.repository.HotelRepository;
import com.nicat.travelbookingsystem.service.HotelService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class HotelServiceImpl implements HotelService {

    private final HotelRepository hotelRepository;

    private final BookingRepository bookingRepository;

    private final ModelMapper modelMapper;

    @Override
    public List<HotelResponse> findAll() {
        return hotelRepository
                .findAll()
                .stream()
                .map(hotel -> modelMapper.map(hotel, HotelResponse.class))
                .collect(Collectors.toList());
    }

    @Override
    @Cacheable(key = "#hotelId", cacheNames = "hotel")
    public HotelResponse findById(Long hotelId) {
        Hotel hotel = hotelRepository.findById(hotelId).orElseThrow(() -> new RuntimeException(
                String.format("Hotel not found by id -%s", hotelId)
        ));

        return modelMapper.map(hotel, HotelResponse.class);
    }

    @Override
    public HotelResponse save(Long bookingId, HotelRequest hotelRequest) {
        Booking booking = bookingRepository.findById(bookingId).orElseThrow(() -> new RuntimeException(
                String.format("Booking not found by id -%s", bookingId)
        ));

        Hotel hotel = modelMapper.map(hotelRequest, Hotel.class);
        hotel.setBooking(booking);

        return modelMapper.map(hotelRepository.save(hotel), HotelResponse.class);
    }

    @Override
    public HotelResponse update(Long hotelId, HotelRequest hotelRequest) {
        Hotel hotel = hotelRepository.findById(hotelId).orElseThrow(() -> new RuntimeException(
                String.format("Hotel not found by id -%s", hotelId)
        ));

        modelMapper.map(hotelRequest, hotel, "map");
        hotel.setId(hotelId);

        return modelMapper.map(hotelRepository.save(hotel), HotelResponse.class);
    }

    @Override
    public void delete(Long hotelId) {
        Hotel hotel = hotelRepository.findById(hotelId).orElseThrow(() -> new RuntimeException(
                String.format("Hotel not found by id -%s", hotelId)
        ));

        hotelRepository.delete(hotel);
    }
}
