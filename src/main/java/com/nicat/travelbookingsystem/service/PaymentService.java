package com.nicat.travelbookingsystem.service;

import com.nicat.travelbookingsystem.dto.request.PaymentRequest;
import com.nicat.travelbookingsystem.dto.response.PaymentResponse;

import java.util.List;

public interface PaymentService {

    List<PaymentResponse> findAll();

    PaymentResponse findById(Long paymentId);

    PaymentResponse save(PaymentRequest paymentRequest);

    PaymentResponse update(PaymentRequest paymentRequest, Long paymentId);

    void delete(Long paymentId);
}
