package com.nicat.travelbookingsystem.entity;

public enum RoomType {
    SINGLE,
    DOUBLE,
    STUDIO,
    DELUXE,
    SUITE,
    PRESIDENTIAL
}
