package com.nicat.travelbookingsystem.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Entity
@FieldDefaults(level = AccessLevel.PRIVATE)
@Table(name = "bookings")
@Builder
public class Booking implements Serializable {

    public static final long serialVersionUID = 1234123456123L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    String bookingDate;

    Double totalCost;

    @ManyToOne
    @JsonIgnore
    @ToString.Exclude
    User user;

    @OneToMany(mappedBy = "booking", cascade = CascadeType.REMOVE)
    @Builder.Default
    List<Flight> flights = new ArrayList<>();

    @OneToMany(mappedBy = "booking", cascade = CascadeType.REMOVE)
    @Builder.Default
    List<Hotel> hotels = new ArrayList<>();

    @OneToMany(mappedBy = "booking", cascade = CascadeType.REMOVE)
    @Builder.Default
    List<RentalCar> rentalCars = new ArrayList<>();

    @OneToOne
    @JoinColumn(name = "payment_id")
    @JsonIgnore
    @ToString.Exclude
    Payment payment;
}
