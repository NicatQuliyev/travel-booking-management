package com.nicat.travelbookingsystem.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.io.Serializable;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Entity
@FieldDefaults(level = AccessLevel.PRIVATE)
@Table(name = "rental-car")
@Builder
public class RentalCar implements Serializable {

    public static final long serialVersionUID = 1234123456123L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    String rentalCompany;

    String carModel;

    String rentalPeriod;

    Double price;

    String pickUpLocation;

    String dropOfLocation;

    @ManyToOne
    @JsonIgnore
    @ToString.Exclude
    Booking booking;
}
