package com.nicat.travelbookingsystem.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.io.Serializable;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Entity
@FieldDefaults(level = AccessLevel.PRIVATE)
@Table(name = "hotels")
@Builder
public class Hotel implements Serializable {

    public static final long serialVersionUID = 1234123456123L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    String hotelName;

    String location;

    @Enumerated(EnumType.STRING)
    RoomType roomType;

    Double price;

    @ManyToOne
    @JsonIgnore
    @ToString.Exclude
    Booking booking;
}
