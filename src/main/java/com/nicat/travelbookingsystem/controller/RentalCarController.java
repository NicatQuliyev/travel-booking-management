package com.nicat.travelbookingsystem.controller;

import com.nicat.travelbookingsystem.dto.request.RentalCarRequest;
import com.nicat.travelbookingsystem.dto.response.RentalCarResponse;
import com.nicat.travelbookingsystem.service.RentalCarService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/api/v1/rental-cars")
@RestController
@RequiredArgsConstructor
public class RentalCarController {

    private final RentalCarService rentalCarService;

    @GetMapping
    public ResponseEntity<List<RentalCarResponse>> findAll() {
        return new ResponseEntity<>(rentalCarService.findAll(), HttpStatus.OK);
    }

    @GetMapping("/{rentalCarId}")
    public ResponseEntity<RentalCarResponse> findById(@PathVariable Long rentalCarId) {
        return new ResponseEntity<>(rentalCarService.findById(rentalCarId), HttpStatus.OK);
    }

    @PostMapping("/booking/{bookingId}")
    public ResponseEntity<RentalCarResponse> save(@PathVariable Long bookingId,
                                                  @RequestBody RentalCarRequest rentalCarRequest) {
        return new ResponseEntity<>(rentalCarService.save(bookingId, rentalCarRequest), HttpStatus.CREATED);
    }

    @PutMapping("/{rentalCarId}")
    public ResponseEntity<RentalCarResponse> update(@PathVariable Long rentalCarId,
                                                    @RequestBody RentalCarRequest rentalCarRequest) {
        return new ResponseEntity<>(rentalCarService.update(rentalCarId, rentalCarRequest), HttpStatus.ACCEPTED);
    }

    @DeleteMapping("/{rentalCarId}")
    public void delete(@PathVariable Long rentalCarId) {
        rentalCarService.delete(rentalCarId);
    }
}
