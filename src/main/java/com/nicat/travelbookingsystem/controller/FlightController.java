package com.nicat.travelbookingsystem.controller;

import com.nicat.travelbookingsystem.dto.request.FlightRequest;
import com.nicat.travelbookingsystem.dto.response.FlightResponse;
import com.nicat.travelbookingsystem.service.FlightService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/api/v1/flights")
@RestController
@RequiredArgsConstructor
public class FlightController {

    private final FlightService flightService;

    @GetMapping
    public ResponseEntity<List<FlightResponse>> findAll() {
        return new ResponseEntity<>(flightService.findAll(), HttpStatus.OK);
    }

    @GetMapping("/{flightId}")
    public ResponseEntity<FlightResponse> findById(@PathVariable Long flightId) {
        return new ResponseEntity<>(flightService.findById(flightId), HttpStatus.OK);
    }

    @PostMapping("/booking/{bookingId}")
    public ResponseEntity<FlightResponse> save(@PathVariable Long bookingId,
                                               @RequestBody FlightRequest flightRequest) {
        return new ResponseEntity<>(flightService.save(bookingId, flightRequest), HttpStatus.CREATED);
    }

    @PutMapping("/{flightId}")
    public ResponseEntity<FlightResponse> update(@PathVariable Long flightId,
                                                 @RequestBody FlightRequest flightRequest) {
        return new ResponseEntity<>(flightService.update(flightId, flightRequest), HttpStatus.ACCEPTED);
    }

    @DeleteMapping("/{flightId}")
    public void delete(@PathVariable Long flightId) {
        flightService.delete(flightId);
    }
}
