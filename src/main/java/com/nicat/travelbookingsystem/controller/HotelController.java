package com.nicat.travelbookingsystem.controller;

import com.nicat.travelbookingsystem.dto.request.HotelRequest;
import com.nicat.travelbookingsystem.dto.response.HotelResponse;
import com.nicat.travelbookingsystem.service.HotelService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/api/v1/hotels")
@RestController
@RequiredArgsConstructor
public class HotelController {

    private final HotelService hotelService;

    @GetMapping
    public ResponseEntity<List<HotelResponse>> findAll() {
        return new ResponseEntity<>(hotelService.findAll(), HttpStatus.OK);
    }

    @GetMapping("/{hotelId}")
    public ResponseEntity<HotelResponse> findById(@PathVariable Long hotelId) {
        return new ResponseEntity<>(hotelService.findById(hotelId), HttpStatus.OK);
    }

    @PostMapping("/booking/{bookingId}")
    public ResponseEntity<HotelResponse> save(@PathVariable Long bookingId,
                                              @RequestBody HotelRequest hotelRequest) {
        return new ResponseEntity<>(hotelService.save(bookingId, hotelRequest), HttpStatus.CREATED);
    }

    @PutMapping("/{hotelId}")
    public ResponseEntity<HotelResponse> update(@PathVariable Long hotelId,
                                                @RequestBody HotelRequest hotelRequest) {
        return new ResponseEntity<>(hotelService.update(hotelId, hotelRequest), HttpStatus.ACCEPTED);
    }

    @DeleteMapping("/{hotelId}")
    public void delete(@PathVariable Long hotelId) {
        hotelService.delete(hotelId);
    }
}
