package com.nicat.travelbookingsystem.dto.request;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class RentalCarRequest {

    private String rentalCompany;

    private String carModel;

    private String rentalPeriod;

    private Double price;

    private String pickUpLocation;

    private String dropOfLocation;
}
