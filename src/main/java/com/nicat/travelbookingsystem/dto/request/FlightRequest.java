package com.nicat.travelbookingsystem.dto.request;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class FlightRequest {
    private String flightNumber;

    private String departureAirport;

    private String arrivalAirport;

    private String departureTime;

    private String arrivalTime;

    private String airline;
}
