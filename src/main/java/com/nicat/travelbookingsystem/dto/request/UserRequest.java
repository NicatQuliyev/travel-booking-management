package com.nicat.travelbookingsystem.dto.request;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class UserRequest {

    private String name;

    private String surname;

    private String email;

    private String phoneNumber;
}
