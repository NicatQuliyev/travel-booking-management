package com.nicat.travelbookingsystem.dto.request;

import com.nicat.travelbookingsystem.entity.RoomType;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class HotelRequest {

    private String hotelName;

    private String location;

    private RoomType roomType;

    private Double price;
}
