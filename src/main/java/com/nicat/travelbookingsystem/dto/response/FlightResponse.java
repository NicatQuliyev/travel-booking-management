package com.nicat.travelbookingsystem.dto.response;

import com.nicat.travelbookingsystem.entity.Booking;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Data
@Builder
public class FlightResponse implements Serializable {

    public static final long serialVersionUID = 1234123456123L;

    private Long id;

    private String flightNumber;

    private String departureAirport;

    private String arrivalAirport;

    private String departureTime;

    private String arrivalTime;

    private String airline;

   // private Booking booking;
}
