package com.nicat.travelbookingsystem.dto.response;

import com.nicat.travelbookingsystem.entity.Booking;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Data
@Builder
public class PaymentResponse implements Serializable {

    public static final long serialVersionUID = 1234123456123L;

    private Long id;

    private Double paymentAmount;

    private String paymentMethod;

    private String paymentDate;

   // private Booking booking;
}
