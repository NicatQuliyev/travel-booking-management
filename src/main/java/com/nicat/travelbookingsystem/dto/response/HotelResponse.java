package com.nicat.travelbookingsystem.dto.response;

import com.nicat.travelbookingsystem.entity.Booking;
import com.nicat.travelbookingsystem.entity.RoomType;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Data
@Builder
public class HotelResponse implements Serializable {

    public static final long serialVersionUID = 1234123456123L;

    private Long id;

    private String hotelName;

    private String location;

    private RoomType roomType;

    private Double price;

  //  private Booking booking;
}
