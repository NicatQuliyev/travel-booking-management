package com.nicat.travelbookingsystem.dto.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserResponse implements Serializable {

//    public static final long serialVersionUID = 1234123456123L;

    private Long id;

    private String name;

    private String surname;

    private String email;

    private String phoneNumber;

    private List<BookingResponse> bookings;
}
