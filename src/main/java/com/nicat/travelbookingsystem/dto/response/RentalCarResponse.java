package com.nicat.travelbookingsystem.dto.response;

import com.nicat.travelbookingsystem.entity.Booking;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Data
@Builder
public class RentalCarResponse implements Serializable {

    public static final long serialVersionUID = 1234123456123L;

    private Long id;

    private String rentalCompany;

    private String carModel;

    private String rentalPeriod;

    private Double price;

    private String pickUpLocation;

    private String dropOfLocation;

  //  private Booking booking;
}
