package com.nicat.travelbookingsystem.dto.response;

import com.nicat.travelbookingsystem.entity.Payment;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
@Builder
public class BookingResponse implements Serializable {

    public static final long serialVersionUID = 1234123456123L;

    private Long id;

    private String bookingDate;

    private Double totalCost;

   // private User user;

    private List<FlightResponse> flights = new ArrayList<>();

    private List<HotelResponse> hotels = new ArrayList<>();

    private List<RentalCarResponse> rentalCars = new ArrayList<>();

    private PaymentResponse payment;

}
