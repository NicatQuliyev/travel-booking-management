package com.nicat.travelbookingsystem.service.impl;

import com.nicat.travelbookingsystem.dto.request.UserRequest;
import com.nicat.travelbookingsystem.dto.response.UserResponse;
import com.nicat.travelbookingsystem.entity.User;
import com.nicat.travelbookingsystem.repository.UserRepository;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cache.Cache;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
@ActiveProfiles("redis")
class UserServiceImpIntegrationTest {

    @Autowired
    UserServiceImpl userService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    ModelMapper modelMapper;

    @Autowired
    private RedisCacheManager cacheManager;


    @Container
    public static PostgreSQLContainer postgreSQLContainer = new PostgreSQLContainer("postgres:latest")
            .withDatabaseName("travel-booking-system-test")
            .withPassword("password")
            .withUsername("postgres");

    @Container
    public static GenericContainer redis = new GenericContainer("redis:latest");


    @DynamicPropertySource
    public static void overrideProperties(DynamicPropertyRegistry registry) {
        registry.add("spring.datasource.url", postgreSQLContainer::getJdbcUrl);
        registry.add("spring.datasource.username", postgreSQLContainer::getUsername);
        registry.add("spring.datasource.password", postgreSQLContainer::getPassword);
        registry.add("spring.datasource.driver-class-name", postgreSQLContainer::getDriverClassName);
    }

    @DynamicPropertySource
    public static void redisProperties(DynamicPropertyRegistry registry) {
        registry.add("spring.redis.host", redis::getHost);
        registry.add("spring.redis.port", redis::getFirstMappedPort);
    }


    @BeforeAll
    public static void setUp() {
        postgreSQLContainer.start();
        redis.start();
    }

    @Test
    void testFindAllComeFromCache() {
        //Arrange
        Cache cache = cacheManager.getCache("users");

        //Act
        List<UserResponse> users = userService.findAll();

        //Assert
        Cache.ValueWrapper cacheResult = cache.get("findAll");
        assertThat(users).isEqualTo(cacheResult.get());
    }

    @Test
    void testGetAccountById() {
        User user = userRepository.save(User.builder()
                .name("Nicat")
                .surname("Quliyev")
                .email("quliyevv.nicat2003@gmail.com")
                .phoneNumber("+994502858581")
                .build());

        UserResponse userResponse = UserResponse.builder()
                .name("Nicat")
                .surname("Quliyev")
                .email("quliyevv.nicat2003@gmail.com")
                .phoneNumber("+994502858581")
                .build();

        modelMapper.map(user, userResponse);

        //Act
        UserResponse byId = userService.findById(user.getId());

        //Assert
        assertThat(byId).isEqualTo(userResponse);
    }

    @Test
    void testCreateAccount() {
        //Arrange
        UserRequest userRequestDto = UserRequest.builder()
                .name("Nicat")
                .surname("Quliyev")
                .email("quliyevv.nicat2003@gmail.com")
                .phoneNumber("+994502858581")
                .build();

        //Act
        var save = userService.save(userRequestDto);

        //Assert
        assertThat(save.getId()).isNotNull();
        assertThat(save.getName()).isEqualTo("Nicat");
        assertThat(save.getSurname()).isEqualTo("Quliyev");

        List<User> all = userRepository.findAll();
        assertThat(all.size()).isEqualTo(1);
        assertThat(all.get(0).getName()).isEqualTo("Nicat");
        assertThat(all.get(0).getSurname()).isEqualTo("Quliyev");
        assertThat(all.get(0).getId()).isEqualTo(save.getId());
    }


    @AfterAll
    public static void tearDown() {
        postgreSQLContainer.stop();
        redis.stop();
    }
}