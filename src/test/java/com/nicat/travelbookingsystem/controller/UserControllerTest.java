package com.nicat.travelbookingsystem.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nicat.travelbookingsystem.dto.request.UserRequest;
import com.nicat.travelbookingsystem.dto.response.UserResponse;
import com.nicat.travelbookingsystem.service.impl.UserServiceImpl;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(UserController.class)
@RunWith(SpringRunner.class)
class UserControllerTest {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    ObjectMapper objectMapper;

    @MockBean
    private UserServiceImpl userService;

    @Test
    @DisplayName(value = "findAll")
    void givenValidUserResponseListThenSuccess() throws Exception {
        //Arrange
        List<UserResponse> userList = new ArrayList<>();

        UserResponse userResponse = UserResponse.builder()
                .id(1L)
                .name("Nicat")
                .surname("Quliyev")
                .email("quliyevv.nicat2003@gmail.com")
                .phoneNumber("+994502858581")
                .build();

        userList.add(userResponse);

        when(userService.findAll()).thenReturn(userList);

        //Act&Assert
        mockMvc.perform(get("/api/v1/users")
                        .accept(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(objAsJson(userList)))
                .andExpect(jsonPath("$[0].id").value("1"))
                .andExpect(jsonPath("$[0].name").value("Nicat"))
                .andExpect(jsonPath("$[0].surname").value("Quliyev"))
                .andExpect(jsonPath("$[0].email").value("quliyevv.nicat2003@gmail.com"))
                .andExpect(jsonPath("$[0].phoneNumber").value("+994502858581"));

    }

    @Test
    @DisplayName(value = "FindById")
    void givenValidIdWhenGetUserThenSuccess() throws Exception {
        //Arrange
        UserResponse mockUserResponse = UserResponse.builder()
                .id(1L)
                .name("Nicat")
                .surname("Quliyev")
                .email("quliyevv.nicat2003@gmail.com")
                .phoneNumber("+994502858581")
                .build();

        when(userService.findById(anyLong())).thenReturn(mockUserResponse);

        //Act&Assert
        mockMvc.perform(get("/api/v1/users/1")
                        .accept(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(objAsJson(mockUserResponse)))
                .andExpect(jsonPath("$.id").value("1"))
                .andExpect(jsonPath("$.name").value("Nicat"))
                .andExpect(jsonPath("$.surname").value("Quliyev"))
                .andExpect(jsonPath("$.email").value("quliyevv.nicat2003@gmail.com"))
                .andExpect(jsonPath("$.phoneNumber").value("+994502858581"));
    }

    @Test
    @DisplayName(value = "Save")
    void givenValidUserRequestDtoWhenCreateThenSuccess() throws Exception {
        //Arrange
        UserRequest userRequestDto = UserRequest.builder()
                .name("Nicat")
                .surname("Quliyev")
                .email("quliyevv.nicat2003@gmail.com")
                .phoneNumber("+994502858581")
                .build();

        UserResponse mockUserResponse = UserResponse.builder()
                .id(1L)
                .name("Nicat")
                .surname("Quliyev")
                .email("quliyevv.nicat2003@gmail.com")
                .phoneNumber("+994502858581")
                .build();

        when(userService.save(any())).thenReturn(mockUserResponse);

        //Act&Assert
        mockMvc.perform(post("/api/v1/users")
                        .contentType(APPLICATION_JSON)
                        .content(objAsJson(userRequestDto))
                        .characterEncoding("UTF-8")
                        .accept(APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id").value("1"))
                .andExpect(jsonPath("$.name").value("Nicat"))
                .andExpect(jsonPath("$.surname").value("Quliyev"))
                .andExpect(jsonPath("$.email").value("quliyevv.nicat2003@gmail.com"))
                .andExpect(jsonPath("$.phoneNumber").value("+994502858581"));

    }

    @Test
    @DisplayName(value = "update")
    void givenValidUserResponseWhenUpdateThenSuccess() throws Exception {
        //Arrange
        UserRequest userRequestDto = UserRequest.builder()
                .name("Nicat")
                .surname("Quliyev")
                .email("quliyevv.nicat2003@gmail.com")
                .phoneNumber("+994502858581")
                .build();

        UserResponse mockUserResponse = UserResponse.builder()
                .id(1L)
                .name("Nicat")
                .surname("Quliyev")
                .email("quliyevv.nicat2003@gmail.com")
                .phoneNumber("+994502858581")
                .build();

        when(userService.update(userRequestDto, 1L)).thenReturn(mockUserResponse);

        //Act&Assert
        mockMvc.perform(put("/api/v1/users/1")
                        .contentType(APPLICATION_JSON)
                        .content(objAsJson(userRequestDto))
                        .characterEncoding("UTF-8")
                        .accept(APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id").value("1"))
                .andExpect(jsonPath("$.name").value("Nicat"))
                .andExpect(jsonPath("$.surname").value("Quliyev"))
                .andExpect(jsonPath("$.email").value("quliyevv.nicat2003@gmail.com"))
                .andExpect(jsonPath("$.phoneNumber").value("+994502858581"));
    }

    @Test
    @DisplayName(value = "delete")
    void givenValidWhenUserDeleteThenSuccess() throws Exception {
        //Arrange
        UserResponse userResponse = UserResponse.builder()
                .id(1L)
                .name("Nicat")
                .surname("Quliyev")
                .email("quliyevv.nicat2003@gmail.com")
                .phoneNumber("+994502858581")
                .build();

        when(userService.findById(userResponse.getId())).thenReturn(userResponse);

        //Act&Assert
        mockMvc.perform(delete("/api/v1/users/1")
                        .accept(APPLICATION_JSON))
                .andExpect(status().isOk());

    }

    private String objAsJson(Object obj) throws JsonProcessingException {
        return objectMapper.writeValueAsString(obj);
    }

}